EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A 11000 8500
encoding utf-8
Sheet 1 1
Title "Procon GCC Button Board"
Date "2019-12-18"
Rev "0.2"
Comp "Luberry's Custom Controllers"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:GND #PWR0132
U 1 1 5DF27336
P 7100 4300
F 0 "#PWR0132" H 7100 4050 50  0001 C CNN
F 1 "GND" H 7105 4127 50  0000 C CNN
F 2 "" H 7100 4300 50  0001 C CNN
F 3 "" H 7100 4300 50  0001 C CNN
	1    7100 4300
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP14
U 1 1 5DF24042
P 7100 4300
F 0 "TP14" V 7100 4488 50  0000 L CNN
F 1 "TestPoint" V 7145 4488 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 7300 4300 50  0001 C CNN
F 3 "~" H 7300 4300 50  0001 C CNN
	1    7100 4300
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP13
U 1 1 5DF24038
P 7100 4200
F 0 "TP13" V 7100 4388 50  0000 L CNN
F 1 "TestPoint" V 7145 4388 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 7300 4200 50  0001 C CNN
F 3 "~" H 7300 4200 50  0001 C CNN
	1    7100 4200
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP12
U 1 1 5DF2402E
P 7100 4100
F 0 "TP12" V 7100 4288 50  0000 L CNN
F 1 "TestPoint" V 7145 4288 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 7300 4100 50  0001 C CNN
F 3 "~" H 7300 4100 50  0001 C CNN
	1    7100 4100
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP11
U 1 1 5DF24024
P 7100 4000
F 0 "TP11" V 7100 4188 50  0000 L CNN
F 1 "TestPoint" V 7145 4188 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 7300 4000 50  0001 C CNN
F 3 "~" H 7300 4000 50  0001 C CNN
	1    7100 4000
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP10
U 1 1 5DF2401A
P 7100 3900
F 0 "TP10" V 7100 4088 50  0000 L CNN
F 1 "TestPoint" V 7145 4088 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 7300 3900 50  0001 C CNN
F 3 "~" H 7300 3900 50  0001 C CNN
	1    7100 3900
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP9
U 1 1 5DF24010
P 7100 3800
F 0 "TP9" V 7100 3988 50  0000 L CNN
F 1 "TestPoint" V 7145 3988 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 7300 3800 50  0001 C CNN
F 3 "~" H 7300 3800 50  0001 C CNN
	1    7100 3800
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP8
U 1 1 5DF22B60
P 7100 3700
F 0 "TP8" V 7100 3888 50  0000 L CNN
F 1 "TestPoint" V 7145 3888 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 7300 3700 50  0001 C CNN
F 3 "~" H 7300 3700 50  0001 C CNN
	1    7100 3700
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP7
U 1 1 5DF22B56
P 7100 3600
F 0 "TP7" V 7100 3788 50  0000 L CNN
F 1 "TestPoint" V 7145 3788 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 7300 3600 50  0001 C CNN
F 3 "~" H 7300 3600 50  0001 C CNN
	1    7100 3600
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP6
U 1 1 5DF22B4C
P 7100 3500
F 0 "TP6" V 7100 3688 50  0000 L CNN
F 1 "TestPoint" V 7145 3688 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 7300 3500 50  0001 C CNN
F 3 "~" H 7300 3500 50  0001 C CNN
	1    7100 3500
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP5
U 1 1 5DF21A60
P 7100 3400
F 0 "TP5" V 7100 3588 50  0000 L CNN
F 1 "TestPoint" V 7145 3588 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 7300 3400 50  0001 C CNN
F 3 "~" H 7300 3400 50  0001 C CNN
	1    7100 3400
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP4
U 1 1 5DF21729
P 7100 3300
F 0 "TP4" V 7100 3488 50  0000 L CNN
F 1 "TestPoint" V 7145 3488 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 7300 3300 50  0001 C CNN
F 3 "~" H 7300 3300 50  0001 C CNN
	1    7100 3300
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP3
U 1 1 5DF213DF
P 7100 3200
F 0 "TP3" V 7100 3388 50  0000 L CNN
F 1 "TestPoint" V 7145 3388 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 7300 3200 50  0001 C CNN
F 3 "~" H 7300 3200 50  0001 C CNN
	1    7100 3200
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP2
U 1 1 5DF21214
P 7100 3100
F 0 "TP2" V 7100 3288 50  0000 L CNN
F 1 "TestPoint" V 7145 3288 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 7300 3100 50  0001 C CNN
F 3 "~" H 7300 3100 50  0001 C CNN
	1    7100 3100
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP1
U 1 1 5DF1F247
P 7100 3000
F 0 "TP1" V 7100 3188 50  0000 L CNN
F 1 "TestPoint" V 7145 3188 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 7300 3000 50  0001 C CNN
F 3 "~" H 7300 3000 50  0001 C CNN
	1    7100 3000
	0    1    1    0   
$EndComp
Text GLabel 7100 3400 0    50   Input ~ 0
BB_BTN_ST
Text GLabel 7100 3000 0    50   Input ~ 0
BB_BTN_A
Text GLabel 7100 3100 0    50   Input ~ 0
BB_BTN_B
Text GLabel 7100 3300 0    50   Input ~ 0
BB_BTN_Y
Text GLabel 7100 3200 0    50   Input ~ 0
BB_BTN_X
Text GLabel 7100 3500 0    50   Input ~ 0
BB_BTN_ZL
Text GLabel 7100 3600 0    50   Input ~ 0
BB_BTN_ZR
Text GLabel 7100 3700 0    50   Input ~ 0
BB_BTN_LT
Text GLabel 7100 3800 0    50   Input ~ 0
BB_BTN_RT
Text GLabel 7100 3900 0    50   Input ~ 0
BB_BTN_DP_U
Text GLabel 7100 4000 0    50   Input ~ 0
BB_BTN_DP_D
Text GLabel 7100 4100 0    50   Input ~ 0
BB_BTN_DP_L
Text GLabel 7100 4200 0    50   Input ~ 0
BB_BTN_DP_R
$Comp
L power:GND #PWR0108
U 1 1 5DE6069F
P 5250 2450
F 0 "#PWR0108" H 5250 2200 50  0001 C CNN
F 1 "GND" V 5255 2322 50  0000 R CNN
F 2 "" H 5250 2450 50  0001 C CNN
F 3 "" H 5250 2450 50  0001 C CNN
	1    5250 2450
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x05_Female J2
U 1 1 5DE5CFE1
P 5450 2450
F 0 "J2" H 5478 2476 50  0000 L CNN
F 1 "bumpers" H 5478 2385 50  0000 L CNN
F 2 "procon_gcc:molex_5051100592_5P" H 5450 2450 50  0001 C CNN
F 3 "~" H 5450 2450 50  0001 C CNN
F 4 "https://www.digikey.com/product-detail/en/molex/5051100592/WM12361TR-ND/5700453" H 5450 2450 50  0001 C CNN "Digikey"
	1    5450 2450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0107
U 1 1 5DE5B4F8
P 5250 3000
F 0 "#PWR0107" H 5250 2750 50  0001 C CNN
F 1 "GND" V 5255 2872 50  0000 R CNN
F 2 "" H 5250 3000 50  0001 C CNN
F 3 "" H 5250 3000 50  0001 C CNN
	1    5250 3000
	0    -1   -1   0   
$EndComp
$Comp
L gcc:procon_abxy SW1
U 1 1 5DE59B12
P 4850 3650
F 0 "SW1" H 5033 4515 50  0000 C CNN
F 1 "procon_abxy" H 5033 4424 50  0000 C CNN
F 2 "procon_gcc:procon_abxy" H 4850 3650 50  0001 C CNN
F 3 "" H 4850 3650 50  0001 C CNN
	1    4850 3650
	1    0    0    -1  
$EndComp
$Comp
L gcc:procon_dpad SW2
U 1 1 5DE5813F
P 4850 4400
F 0 "SW2" H 5033 5265 50  0000 C CNN
F 1 "procon_dpad" H 5033 5174 50  0000 C CNN
F 2 "procon_gcc:procon_dpad" H 4850 4400 50  0001 C CNN
F 3 "" H 4850 4400 50  0001 C CNN
	1    4850 4400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0106
U 1 1 5DE5B45F
P 5250 3750
F 0 "#PWR0106" H 5250 3500 50  0001 C CNN
F 1 "GND" V 5255 3622 50  0000 R CNN
F 2 "" H 5250 3750 50  0001 C CNN
F 3 "" H 5250 3750 50  0001 C CNN
	1    5250 3750
	0    -1   -1   0   
$EndComp
Text GLabel 4750 4600 0    50   Input ~ 0
BB_BTN_ST
Text GLabel 5250 3100 2    50   Input ~ 0
BB_BTN_A
Text GLabel 5250 3200 2    50   Input ~ 0
BB_BTN_B
Text GLabel 5250 3300 2    50   Input ~ 0
BB_BTN_Y
Text GLabel 5250 3400 2    50   Input ~ 0
BB_BTN_X
Text GLabel 5250 2350 0    50   Input ~ 0
BB_BTN_ZL
Text GLabel 5250 2550 0    50   Input ~ 0
BB_BTN_ZR
Text GLabel 5250 2250 0    50   Input ~ 0
BB_BTN_LT
Text GLabel 5250 2650 0    50   Input ~ 0
BB_BTN_RT
Text GLabel 5250 3850 2    50   Input ~ 0
BB_BTN_DP_U
Text GLabel 5250 4050 2    50   Input ~ 0
BB_BTN_DP_D
Text GLabel 5250 4150 2    50   Input ~ 0
BB_BTN_DP_L
Text GLabel 5250 3950 2    50   Input ~ 0
BB_BTN_DP_R
Text GLabel 3600 3400 0    50   Input ~ 0
BB_BTN_ST
Text GLabel 3600 3000 0    50   Input ~ 0
BB_BTN_A
Text GLabel 3600 3100 0    50   Input ~ 0
BB_BTN_B
Text GLabel 3600 3300 0    50   Input ~ 0
BB_BTN_Y
Text GLabel 3600 3200 0    50   Input ~ 0
BB_BTN_X
Text GLabel 3600 3500 0    50   Input ~ 0
BB_BTN_ZL
Text GLabel 3600 3600 0    50   Input ~ 0
BB_BTN_ZR
Text GLabel 3600 3700 0    50   Input ~ 0
BB_BTN_LT
Text GLabel 3600 3800 0    50   Input ~ 0
BB_BTN_RT
Text GLabel 3600 3900 0    50   Input ~ 0
BB_BTN_DP_U
Text GLabel 3600 4000 0    50   Input ~ 0
BB_BTN_DP_D
Text GLabel 3600 4100 0    50   Input ~ 0
BB_BTN_DP_L
Text GLabel 3600 4200 0    50   Input ~ 0
BB_BTN_DP_R
$Comp
L power:GND #PWR0105
U 1 1 5DDE152A
P 5150 4600
F 0 "#PWR0105" H 5150 4350 50  0001 C CNN
F 1 "GND" H 5155 4427 50  0000 C CNN
F 2 "" H 5150 4600 50  0001 C CNN
F 3 "" H 5150 4600 50  0001 C CNN
	1    5150 4600
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW3
U 1 1 5DDDEE01
P 4950 4600
F 0 "SW3" H 4950 4885 50  0000 C CNN
F 1 " TL3315NF100Q" H 4950 4794 50  0000 C CNN
F 2 "procon_gcc:TL3315NFxxxQ" H 4950 4800 50  0001 C CNN
F 3 "https://media.digikey.com/pdf/Data%20Sheets/E-Switch%20PDFs/TL3315.pdf" H 4950 4800 50  0001 C CNN
F 4 "https://www.digikey.com/product-detail/en/e-switch/TL3315NF100Q/EG4620CT-ND/1870400" H 4950 4600 50  0001 C CNN "Digikey"
	1    4950 4600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0104
U 1 1 5DDE1F4C
P 3600 4300
F 0 "#PWR0104" H 3600 4050 50  0001 C CNN
F 1 "GND" H 3605 4127 50  0000 C CNN
F 2 "" H 3600 4300 50  0001 C CNN
F 3 "" H 3600 4300 50  0001 C CNN
	1    3600 4300
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x14 J1
U 1 1 5DDDBC66
P 3800 3600
AR Path="/5DDDBC66" Ref="J1"  Part="1" 
AR Path="/5DDDA801/5DDDBC66" Ref="J3"  Part="1" 
F 0 "J1" H 3880 3592 50  0000 L CNN
F 1 "Button Board" H 3880 3501 50  0000 L CNN
F 2 "procon_gcc:molex_5051101492_14P" H 3800 3600 50  0001 C CNN
F 3 "https://www.molex.com/pdm_docs/sd/5051101492_sd.pdf" H 3800 3600 50  0001 C CNN
F 4 "https://www.digikey.com/product-detail/en/molex/5051101492/WM12370CT-ND/5726229" H 3800 3600 50  0001 C CNN "Digikey"
	1    3800 3600
	1    0    0    -1  
$EndComp
$EndSCHEMATC
